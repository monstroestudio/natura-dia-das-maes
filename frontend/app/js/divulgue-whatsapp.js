import Utils from './utils/utils';
import Cookie from './utils/cookie';
import Modal from './modal';
import SalvarCn from './salvar-cn.js';
import ScrollAnchor from './utils/scroll-anchor.js';

/**
 Classe que controla a sessao divulgue
 no facebook. Faz o controle de exibicao
 com o dropbox, paginacao e compartilha
 na timeline do usuario
 @class DivulgueWhatsApp
*/
var DivulgueWhatsApp = {
  apiAvailable: false,
  whatsAppMustDownload: true,
  postsSelector: '#wpCustomPosts',
  showMoreSelector: '#showMoreWhats',
  itemsPerPage: 3,
  init: function () {
    window.Cookie = Cookie;
    this.setEls();
    this.showMore();
    this.share();
    this.getServiceNumber();
    this.applyMaskForm('#formNumWp');
    this.sortWord();
    if (Helper.onlyMobile()) {
      //this.setCarousel();
    }
  },

  sortWord: function(){
    $(function(){
      var r = Math.floor(Math.random()*1000).toString();
      var t = "NATURA-" + r;
      $('#lbl-natura-palavra').text(t);
    });
  },

  setEls: function() {
    this.$whatsPassos = $('#whatsPassos');
    this.$phoneNatura = $("#phoneNatura");
    this.$modal = $('#scModal');
  },

  /**
   Exibe mais posts que estao disponiveis na pagina
   @method showMore
  */
  showMore: function() {
    var _that = this,
        $items = $(this.postsSelector + ' .sc-item[data-item]');
    $(document).on('click', this.showMoreSelector, function() {
      var count = 0;
      if ($items) {
        $items.each(function() {
          if ($(this).hasClass('sc-none')) {
            $(this).removeClass('sc-none');
            count++;
            if (count >= _that.itemsPerPage) {
              count = 0;
              return false;
            }
          }
        });
        if (!_that.getTotalItemsLeft())
          _that.hideShowMore();
      }
    });
  },

  /**
   Retorna o total de items que precisam ser carregados
   @method getTotalItemsLeft
  */
  getTotalItemsLeft: function() {
    var $itemsLeft = $(this.postsSelector + ' .sc-none.sc-item');
    return $itemsLeft.length;
  },

  /**
   Esconde botao de exibir mais posts
   @method hideShowMore
  */
  hideShowMore: function() {
    $(this.showMoreSelector).addClass('sc-none');
  },

  /**
   Exibe botao de exibir mais posts
   @method showShowMore
  */
  showShowMore: function() {
    $(this.showMoreSelector).removeClass('sc-none');
  },

  forceDownload: function(url, filename){
      var link = document.createElement('a');
      link.href = url;
      link.download = filename;
      document.body.appendChild(link);
      link.click();
  },

  shareDownload: function(_that, button){
    var $father = $(button).parents('.sc-item');
    _that.setPostData($father);
    var _data = Utils.getHash(Utils.getCookieData());
    var _postid = _that.postSelected.id;
    var _goo    = $('#customLink').find('.sc-form-ipt').val();
    trackAnalytics(false, 'whats-pagina', 'download_sucesso-' + _postid);
    var _imgurl = PersistData.serviceUrl + "?goo="+ _goo +"&post=" + _postid.toString() + "&data=" + _data + "&download=true";
    var _filename = "Natura_" + _postid;
    _that.forceDownload(_imgurl, _filename);
    return true;
  },

  shareWhatsApp: function(_that, button){
    var $father = $(button).parents('.sc-item');
    _that.setPostData($father);
    if (!$(_that).hasClass('loading')) {
      _that.clearLoadingShare();
      $(_that).addClass('loading');
      if (_that.isValidRequest()) {
        var data = {},
            cookieNumer = _that.getCookieNumber(),
            $iptNum = $('#formNumWp');
        _that.clientNumber = $iptNum.val();
        // data.clientNumber = cookieNumer.clientNumber;
        data.clientNumber = '55'+Utils.clearPhoneNumber(_that.clientNumber);
        data.post = _that.getPost("id");
        data.data = Utils.getHash(Utils.getCookieData());
        _that.sendData(data, 'whats-pagina').then(function(){
          $(button).prop("disabled",true);
        });
      } else {
        trackAnalytics(false, 'whats-popup', 'confirmar-numero');
        Modal.open({
          el: '#formMissingNumber',
          callback: function() {
            $('#iptMissNum').val($('#formNumWp').val());
            _that.applyMaskForm('#iptMissNum');
            _that.sendMissNumber();
          }
        });
      }
    }
    console.log('DivulgueWhatsApp /shareWhatsApp');
  },

  missingOnlyPersonalisation: function(){
    var _missingPersonalisation =
        true === this.defineMustDownload() &&
        true  === this.apiAvailable &&
        false === Helper.isMobile.any() &&
        false === this.isPersonalizado();
    return _missingPersonalisation;
  },

  /**
   Compartilha post customizado no whatsapp
   @method share
  */
  share: function() {
    var _that = this;
    $(document).on('click', this.postsSelector + ' .sc-item .sc-btn', function() {
      /**
       * Share de WhatsApp
       * 3 opcoes
       * 1) tudo OK, enviar para WhatsApp
       * 2) falta apenas a personalizacao, encaminhar para
       * 3) download, servico WhatsApp nao disponivel
       */

      // 1) tudo OK, enviar para WhatsApp
      var sendToWhatsApp = (false === _that.defineMustDownload());
      if (true === sendToWhatsApp) {
        _that.shareWhatsApp(_that, this);
        return;
      }

      // 2) falta apenas a personalizacao.
      if (_that.missingOnlyPersonalisation()) {
        trackAnalytics(false, 'whats-popup', 'voltar-passo-1');
        ScrollAnchor.go('#passo01');
        var _msg = "Você precisa voltar ao passo 1 e personalizar seu material de divulgação para receber as imagens via WhatsApp.";
        Modal.open({
            el: '#formWhatsGeneric',
            autoClose: 10000,
            init: function($el) {
              $el.find('#whats-replace').text(_msg);
            }
        });
        return;
      }

      // 3) download
      _that.shareDownload(_that, this);
    });
  },

  setPostData: function($post) {
    this.postSelected = {
      items: $post.data('item').split(' '),
      id: $post.data('post')
    };
  },

  getPost: function(item) {
    var item = item || null;
    if (!item)
      return this.postSelected;

    return this.postSelected[item];
  },

  /**
   Remove classe loading dos botoes de compartilhamento
   @method clearAllFilterPosts
  */
  clearLoadingShare: function() {
    $(this.postsSelector + ' .sc-item .sc-btn').removeClass('loading');
  },

  /**
   Verifica se o usuario preencheu todas 
   as informacoes necessarias armazenadas no
   cookie natura_natal
   @method isValidRequest
  */
  isValidRequest: function() {
    // Quando for apenas download, este metodo pode retornar
    // sempre true.
    if (true === this.whatsAppMustDownload) {
      return true;
    }

    var value = $('#formNumWp').val(),
        cookieNumber = this.getCookieNumber();

    if (typeof cookieNumber.clientNumber !== "undefined") {
      var currentNumber = "55" + Utils.clearPhoneNumber(value);
      if (cookieNumber.clientNumber !== currentNumber) {
        return false;
      }
    }

    if(value.length) {
      if (value.length >= 14 && value.length <= 15)
        return true
      else
        return false
    } else
      return false
  },

  applyMaskForm: function(selector) {
    var $formNumCont = $(selector);
    console.log($formNumCont);
    var options =  {onKeyPress: function(formNumCont, e, field, options){
      var masks = ['(00) 000000000', '(00) 00000000'],
          mask = (formNumCont.length == 14) ? masks[0] : masks[1];
    }};
    $formNumCont.mask('(00) 000000000');
  },

  sendMissNumber: function() {
    var _that = this;
    var _form = $('#formMissing');
    _form.submit(function(e) {
        e.preventDefault();
    }).validate({
      rules: {
        telefone: {
          required: true,
          minlength: 13,
          maxlength: 14
        }
      },
      errorPlacement: function(error, element) {
        // console.log(error, element);
        // TODO: verificar
        // trackAnalytics(false, 'whats-popup', 'cadastro-numero_erro')
      },
      submitHandler: function(form) {
        // form
        var data = {},
            cookie = Utils.getCookieData(),
            cookieData = cookie ? cookie : {};

        _that.$modal.addClass('loading');
        _that.clientNumber = $('#iptMissNum').val();
        $('#formNumWp').val(this.clientNumber);

        data.clientNumber = "55" + Utils.clearPhoneNumber(_that.clientNumber);
        data.post = _that.getPost("id");
        data.data = Utils.getHash(cookieData);

        data.goo  = cookieData ? cookieData.goo : "";
        // console.log(data);
        _that.sendData(data, 'whats-popup');
      }
    });
    var _btn = $('#sendMissNum');
    _btn.click(function(){
      var _valid = _form.valid();
      if (_valid) {
        _form.submit();
      } else {
        trackAnalytics(false, 'whats-popup', 'cadastro-numero_erro')
      }
    });
  },

  sendData: function(data, gaaction, callback) {
    SalvarCn.saveData({whats:data.clientNumber});
    var _that = this,
        callback = callback || function() {};
    var _goo = '';
    try {
      _goo = $('#customLink').find('.sc-form-ipt').val().trim();
      if (_goo != '') {
        var _idx = _goo.lastIndexOf('/');
        if (_idx >= 0) {
          _goo = _goo.substr(_idx + 1);
        }
      }
    } catch (ex) {
    }

    // Fill extra params
    data.nocache = new Date().getTime().toString();
    data.goo     = _goo;

    return $.ajax({
      url: PersistData.serviceUrlWhats + "/sendimage",
      type: 'post',
      data: JSON.stringify(data),
      dataType: 'json',
      success: function(response) {
        if (!!response) {
          if (response.errorMessage) {
            console.log(response);
            console.log('erro!', response.errorMessage);
            _that.$modal.removeClass('loading');
            trackAnalytics(false, gaaction, 'compartilhe_erro-'+ data.post);
            var numeroInativo = (response.errorMessage =='Confirme seu Whatsapp primeiro.');
            if (numeroInativo){
              trackAnalytics(false, gaaction, 'cadastro-numero_inativo');
              Modal.open({
                el: '#formActive',
                callback: function() {
                  console.log('trocou modal :: confirma number');
                  $('#formActive .sc-btn').unbind('click').bind('click', function() {
                    _that.$modal.addClass('loading');
                    _that.sendData(data, 'whats-popup');
                  });
                }
              });
            } else{
              var msgErro = "Whatsapp está temporariamente fora do ar. Tente novamente mais tarde.";
              var erroIntervalo = (response.errorMessage=='Imagem solicitada em menos de 15 minutos');
              if (erroIntervalo){
                msgErro = "Você já solicitou esta imagem no seu Whatsapp, e só poderá solicitá-la novamente depois de 15 minutos.";
                trackAnalytics(false, gaaction, 'intervalo_' + data.post);
              }
              Modal.open({
                el: '#formWhatsGeneric',
                autoClose: 7000,
                init: function($el) {
                  $el.find('#whats-replace').text(msgErro);
                }
              });
            }
          } else {
            // Sucesso
            Modal.open({
              el: '#formWhatsSuccess',
              autoClose: 7000,
              callback: function() {
              }
            });

            trackAnalytics(false, gaaction, 'compartilhe_sucesso-'+data.post);

            var cookie = Cookie.get('natura_natal'),
                number = "55" + Utils.clearPhoneNumber(_that.clientNumber);
            if (!cookie) {
              Cookie.addItem('natura_natal', {
                whats_number: number
              }); 
            } else {
              if (typeof cookie.whats_number === "undefined") {
                Cookie.addItem('natura_natal', {
                  whats_number: number
                }); 
              }
            }

            var $iptNum = $('#formNumWp');
            if (!$iptNum.val().length)
              $iptNum.val(Utils.formatPhoneNumber(number));


            setTimeout(function() {
              if (!_that.$modal.hasClass('sc-none')) {
                //Modal.close(function() {
                //  _that.$modal.addClass('loading');
                //});
              }
              _that.clearLoadingShare();
              // Modal.close(function() {
                
              // });
            }, 1500);
          }
        } else {
          console.log('erro!');
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        if (window.console) 
          console.log("sendMissNumber", textStatus);
      }
    });
  },

  isPersonalizado: function(){
    var data = '';
    try {
      data = Utils.getHash(Utils.getCookieData()).trim();
    } catch (ex) {
      data = '';
    }
    data = data.trim();

    // Verifica o modo de execucao.
    var is_personalizado = data != '';
    return is_personalizado;
  },

  defineMustDownload: function(){
    try {
      /**
       * Configuracao de WhatsApp. Temos 4 situacoes diferentes.
       * 1) [DOWNLOAD] Em mobile, somente download
       * 2) [DOWNLOAD] Api nao tem numeros disponiveis
       * 3) [DOWNLOAD] Sem personalizacao, apenas download
       * 4) [SENDWHATS] Api disponivel e personalizacao OK.
       * Esta decisao afeta o momento de download que, por sua vez,
       * afeta a validacao se a requisicao e valida.
       * */
      // Le os dados personalizados
      var mustDownload =
          false === this.apiAvailable ||
          true  === Helper.isMobile.any() ||
          false === this.isPersonalizado();
      this.whatsAppMustDownload = mustDownload;
      console.log('DivulgueWhatsApp defineMustDownload',
          'apiAvailable', this.apiAvailable,
          'isMobile', Helper.isMobile.any(),
          'isPersonalizado', this.isPersonalizado(),
          'mustDownload', mustDownload);
    } catch (ex) {
      console.log('DivulgueWhatsApp defineMustDownload', ex);
      this.whatsAppMustDownload = true;
    }
    return this.whatsAppMustDownload;
  },

  configureWhatsApp: function(apiAvailable){
    // Atualiza o status e manipula os objetos html
    this.apiAvailable = apiAvailable;
    console.log('DivulgueWhatsapp configureWhatsApp', apiAvailable);
    if (true === apiAvailable) {
      trackAnalytics(false, 'visualizar', 'whatsapp');
      $('#whatsapp-download').hide();
      $('#whatsapp-message').show();
      $('.btn-whatsmsg').html('QUERO RECEBER VIA WHATSAPP');
    } else {
      trackAnalytics(false, 'visualizar', 'download');
      $('#whatsapp-download').show();
      $('#whatsapp-message').hide();
      $('.btn-whatsmsg').html('CLIQUE AQUI PARA BAIXAR IMAGEM');
    }
    this.defineMustDownload();
  },

  getServiceNumber: function() {
    var _that = this,
        cookieNumber = this.getCookieNumber();

    if (typeof cookieNumber.clientNumber === "undefined")
      cookieNumber.clientNumber = "";

    cookieNumber.nocache = new Date().getTime().toString();

    $.ajax({
      url: PersistData.serviceUrlWhats + "/getnextnumber",
      type: 'post',
      data: JSON.stringify(cookieNumber),
      dataType: 'json',
      success: function(response) {
        if (!!response) {
          if (response.errorMessage) {
            console.log('erro!', response.errorMessage);
            // Whatsapp indisponivel. Tente novamente mais tarde.
            //$('.whatsapp-service').hide();
            _that.configureWhatsApp(false);

          } else if (response.deviceNumber) {
            _that.configureWhatsApp(true);
            $('.whatsapp-service').show();
            _that.deviceNumber = response.deviceNumber;
            _that.currentNumber = response.deviceNumber;
            _that.setCurrentPhone(_that.currentNumber);
            if (response.confirmed) {
              var number = Cookie.get('natura_natal').whats_number;
              $('#formNumWp').val(Utils.formatPhoneNumber(number));
              _that.clientNumber = number;
            }
          } else {
            _that.configureWhatsApp(false);
          }
        } else {
          _that.configureWhatsApp(false);
          console.log("error!");
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        _that.configureWhatsApp(false);
        if (window.console) 
          console.log("getServiceNumber", textStatus);
      }
    });
  },

  getCookieNumber: function() {
    var cookie = Cookie.get('natura_natal');
    if (!cookie)
      return {}

    if (typeof cookie.whats_number !== "undefined") {
      return {
        clientNumber: cookie.whats_number
      }
    } else {
      return {};
    }
  },

  setCurrentPhone:function() {
    var number = Utils.formatPhoneNumber(this.currentNumber);
    this.$phoneNatura.html(number);
    $('.num-to-activate').html(number)
  },

  /** Carrossel para a versao mobile */
  setCarousel: function() {
    this.$whatsPassos.slick({
      infinite: false,
      dots: true,
      arrows: false,
      mobileFirst: true,
      adaptiveHeight: false
    });
  }
};

export default DivulgueWhatsApp;



