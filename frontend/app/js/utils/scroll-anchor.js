/**
 Classe que rola a pagina para as sessoes
 @class ScrollAnchor; 
*/
var ScrollAnchor = {
  speed: 500,
  init: function() {
    this.bind();
  },

  bind: function() {
    var _that = this;
    $(document).on('click', '[data-anchor-go]', function() {
      _that.go($(this).data('anchor-go'));
    });
  },

  /**
   Faz o scroll da pagina ate o alvo
   @method go
   @param {string} target Seletor do objeto alvo
  */
  go: function(target, callback) {
    var $el = $(target),
        callback = callback || function() {};
    if (!$el.length)
      return false;

    var y = $el.offset().top;
    $('html, body').animate({
      scrollTop: y
    }, this.speed, function() {
      callback();
    });
  }
};
export default ScrollAnchor;