var Cookie = {
  set: function (name, data) {
    var _cookieValue = name + "=" + JSON.stringify(data) + "; path=/";
    document.cookie=_cookieValue;
  },
  get: function(name) {
    var cookie = document.cookie;
    if (!cookie.length)
      return false;
    var cookies = cookie.split('; '),
        _return = null;
    $.each(cookies, function() {
      var data = this.split('=');
      if (name == data[0]) {
        _return = data[1];
        return null;
      }
    });
    return JSON.parse(_return);
  },
  addItem: function(name, items) {
    if (this.get(name)) {
      var cookie = this.get(name);
      $.each(items, function(k, v) {
        cookie[k] = v;
      });
      this.set(name, cookie);
    } else {
      this.set(name, items);
    }
  },
  clear: function( name ) {
    document.cookie = name + '=; Path=/;';
  }  
};
export default Cookie;