var GoogleApiUrl = {
  getShordUrl: function(params) {
    var success = params.success || function() {},
        error = params.error || function() {},
        _data = JSON.stringify({"longUrl": params.url});

    $.ajax({
        url: this.getUrlService(),
        method: 'POST',
        contentType: 'application/json',
        processData: false,
        data: _data,
        success: function(data, textStatus, jqXHR){

            // Set background
            if (!!data) {
              success(data.id);
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            if (window.console) console.log('error', errorThrown, jqXHR);
            error(jqXHR.responseText);
        }
    });
  },
  getUrlService: function(url) {
    return "https://www.googleapis.com/urlshortener/v1/url?key=" + PersistData.googleApiKey;
  }
};
export default GoogleApiUrl;
