function YoutubeVideo(opts) {
  this.el = opts.el;
  this.$originalEl = $(this.el);
  this.videoid = this.$originalEl.data('videoid');
  this.playerVars = opts.playerVars || {};
  this.tagvideo = this.$originalEl.data('tagvideo') || null;
  this.alreadyPlay = false;
  this.callbackEndVideo = opts.callbackEndVideo || function() {};
  this.boolProgress = {
    bool25: false,
    bool50: false,
    bool75: false
  };
  this.init();
}

YoutubeVideo.prototype.init = function() {
  var _that = this;
  if (typeof YT === "undefined") {  
    var tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    window.onYouTubeIframeAPIReady = function() {
      _that.createPlayer();
    }
  } else {
    this.createPlayer();
  }
};

YoutubeVideo.prototype.createPlayer = function() {
  console.log('Creating player', this);
  var _that = this;
  this.playerVars['wmode'] = 'transparent';
  this.player = new YT.Player('vh-player', {
    videoId: this.videoid,
    wmode: 'transparent',
    playerVars: this.playerVars,
    events: {
      'onReady': onPlayerReady,
      'onStateChange': onPlayerStateChange
    }
  });

  function onPlayerReady(data) {
    _that.onPlayerReady(data);
  }

  function onPlayerStateChange(event) {
    _that.onPlayerStateChange(event);
  }
};

YoutubeVideo.prototype.onPlayerReady = function(data) {
  console.log('YoutubeVideo :: onPlayerReady');
  this.videoData = this.player.getVideoData();
};

YoutubeVideo.prototype.onPlayerStateChange = function(event) {
  console.log('YoutubeVideo :: onPlayerStateChange ' + event.data);
  var title = Helper.strings.formatSlug(this.videoData.title);
  switch (event.data) {
    case YT.PlayerState.ENDED:
      this.stopVideoProgress();
      if (this.tagvideo) {
        trackAnalytics(this.tagvideo, 'video-' + title, 'completo');
      }
      this.callbackEndVideo();
      break;
    case YT.PlayerState.PLAYING:
      this.startVideoProgress();
      if (this.tagvideo && !this.alreadyPlay) {
        trackAnalytics(this.tagvideo, 'video-' + title, 'play');
        this.alreadyPlay = true;
      }
      break;
    case YT.PlayerState.PAUSED:
      this.stopVideoProgress();
      console.log('pause video');
      break;
    case YT.PlayerState.BUFFERING:
      break;
    case YT.PlayerState.CUED:
      break;
  };
};

YoutubeVideo.prototype.stopVideoProgress = function() {
  if(this.progress) {
    clearInterval(this.progress);
  }
};

YoutubeVideo.prototype.startVideoProgress = function() {
  var _that = this,
      duration = this.player.getDuration(),
      title = Helper.strings.formatSlug(this.videoData.title),
      percent;

  this.progress = setInterval(function() {
    percent = _that.player.getCurrentTime() / duration;
    if(!_that.boolProgress.bool25 && percent > 0.25) {
      _that.boolProgress.bool25 = true;
      console.log('25% do video');
    } else if(!_that.boolProgress.bool50 && percent > 0.50) {
      _that.boolProgress.bool50 = true;
      if (_that.tagvideo) {
        trackAnalytics(_that.tagvideo, 'video-' + title, 'meio');
      }
      console.log('50% do video');
    } else if(!_that.boolProgress.bool75 && percent > 0.75) {
      _that.boolProgress.bool75 = true;
      console.log('75% do video');
    }
  }, 200);
};

YoutubeVideo.prototype.destroy = function() {
  this.player.destroy();
};

export default YoutubeVideo;