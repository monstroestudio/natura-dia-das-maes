var Facebook = {
   albumId: null,

   init: function() {
      var _that = this;
      this.loadtag();
      window.fbAsyncInit = function() {
         _that.fbAsyncInit();
      };
   },

   /**
   Carrega a tag do youtube
   @method loadtag
   */
   loadtag: function() {
      if (typeof FB === "undefined") {
         (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/pt_BR/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
          }(document, 'script', 'facebook-jssdk'));
      }
   },

   fbAsyncInit: function() {
      FB.init({
         appId: PersistData.fb_id,
         status: true,
         xfbml: true,
         version: 'v2.4'
      });
   },

   me: function(data) {
      var _that = this;
      var callback = data.callback || function() {};
      var fail = data.fail || function (){};

      this.redir = data.redir;

      FB.getLoginStatus(function(response) {
         if (response.status === 'connected') {
            _that.verificarPermissao(function() {
               _that.postImage(data, _that.token, callback);
            });
            // console.log(currentPost);
         } else {
            FB.login(function(response) {
               if (response.authResponse) {
                  // console.log('response:', response.authResponse);
                  _that.token = response.authResponse.accessToken;

                  FB.api("/me",function (response) {
                     if (response && !response.error) {
                        // fb.data = response;
                        if (typeof response.link !== "undefined")
                           _that.faceProfileUrl = response.link;

                        _that.verificarPermissao(function() {
                           _that.postImage(data,_that.token, callback);
                        },fail);
                     } else {
                        // currentShareButton.parents('.bp-footer').removeClass('actived');
                        fail(response);
                        alert ('Você precisa aceitar o aplicativo para compartilhar');
                     }
                  });
               } else {
                  fail();
               }
            }, {scope: 'user_photos, publish_actions'});
         }
      });
   },

   verificarPermissao: function (callback, fail) {

      FB.api('/me/permissions', function(response) {
         if (!response || response.error) {
            fail(response);
            alert ('Houve um erro ao compartilhar, tente novamente.');
            // trackAnalytics('movimento-toolkit-ciclo-11', data.tag, 'share-erro');
         } else {
            if (response.data.length) {
               var hasDeclined = false;

               $.each(response.data, function(i) {
                  if (this.status=='declined'){
                     FB.login(function(response) { callback() }, {scope: this.permission, auth_type: 'rerequest'});
                     hasDeclined = true;
                  }
               });

               if (!hasDeclined) {
                  callback();
               }
               else{
                  fail();
               }
            }
         }
      });
   },

   postImage: function(data, token, callback, fail) {
      var _that = this;
      var callback = callback || function() {};

      fail = fail  || function (){ };

      this.getAlbumId(data, success);

      function success(albumId){
         FB.api('/' + _that.albumId + '/photos', 'post', {
            access_token: token,
            url: data.picture
         }, faceSuccess);

         function faceSuccess (response) {
            var action = 'post';
            // action = data.tag.length ? (action + '-' + data.tag) : action;
            if (!response || response.error) {
               fail(response);
               alert ('Houve um erro ao compartilhar, tente novamente.');
               // trackError(data);
            } else {
               if (_that.redir) {
                  _that.getFaceProfileUrl(function(action) {
                     if (action) {
                        response.fb_url = _that.faceProfileUrl;
                        callback(response);
                     } else {
                        callback(response);
                     }
                  });
                  callback(response);
               } else {
                  callback(response);
               }

               // trackAnalytics(_TRACK_CAT_, action, 'share-sucesso');
               // openModal($('.success-share'));
               // alert ('Post publicado com sucesso!');
            }
         }
      }
   },

   getFaceProfileUrl: function(callback) {
      var _that = this;
      var callback = callback || function() {};

      FB.api("/me?fields=link",function (response) {
         if (response && !response.error) {
            // fb.data = response;
            if (typeof response.link !== "undefined")
               _that.faceProfileUrl = response.link;
            callback(true);
         } else {
            callback(false);
         }
      });
   },

   getAlbumId: function (data, callback){
      var _that = this;
      var callback = callback || function(){};

      FB.api('/me/albums', function(response) {
         if (!response || response.error) {
            alert ('Houve um erro ao compartilhar, tente novamente.');
            // trackError(data);
            // clearAllFooters();
         } else {
            for (var i=0; i<response.data.length; i++) {
               if (response.data[i].name == PersistData.fbAlbumName) {
                  _that.albumId = response.data[i].id;
               }
            }

            if (!_that.albumId) {
               return _that.criarAlbum(data, callback);
            } else {
               return callback(_that.albumId);
            }
         }
      });
   },

   criarAlbum: function (data, callback){
      FB.api('/me/albums', 'post', {
         name: PersistData.fbAlbumName
      }, function(response) {
         if (!response || response.error) {
            if (response.error.code == 10) {
               alert('Voce deve aceitar a permissão de fotos para conseguir compartilhar!');
            } else {
               alert('Houve um erro ao compartilhar, tente novamente.');
            }
            // trackError(data);
            // clearAllFooters();
         } else {
            //alert('Album criado com sucesso!');
            // console.log('retorno do album criado', response);
            return callback(response.id);
         }
      });
   },

   share: function(url, postId, that, father){

      console.warn('Facebook: share');
      console.log(url);

      FB.ui({
         method: 'share',
         app_id: PersistData.fb_id,
         href: url.toString().replace('image', 'www/share'),
         picture: url,
         success: function (response) {
            Modal.open({
               el: '#formShareSuccess',
               autoClose: 2500,
               callback: function () {
               }
            });

            trackAnalytics(false, 'facebook', 'compartilhe_sucesso-' + postId);
            that.clearLoadingShare();

            if (father.data('cover')) {
               if (typeof response.id !== "undefined" && typeof response.fb_url !== "undefined") {
                  window.open(response.fb_url + "?preview_cover=" + response.id);
               }
            }
         },
         fail: function () {
            trackAnalytics(false, 'facebook', 'compartilhe_erro-' + postId);
            that.clearLoadingShare();
         }
      });
   }
};
export default Facebook;
