import Cookie from './utils/cookie';
import ScrollAnchor from './utils/scroll-anchor';
import Utils from './utils/utils';
import GoogleApiUrl from './utils/google-api-url';
import Preload from './utils/preload';
import Modal from './modal';
import SalvarCn from './salvar-cn.js';

var MaterialDivugacao = {
   lastError: false,
   lastErrorEl: false,
   redeexp: /^((http:\/\/){0,1}rede\.{0,1}natura\.net\/espaco\/){0,1}([a-z0-9]{1,30}){1}\/{0,1}$/i,
   init: function() {
      this.setEls();
      this.validaName();
      this.validaRede();
      this.applyMaskForm();
      this.send();
      this.fillDataIfExist();
      this.bindControls();
   },

   matchRede: function(){
      var m = this.redeexp.exec(this.$rede.val());
      if (m == null) {
         return null;
      }
      return m[m.length - 1];
   },

   bindControls: function(){
      // It will validate the form only when the button is clicked
      var _that = this;

      this.$btnSend.click(function() {
         var _valid = _that.$form.valid();
         if (_valid) {
            _that.$form.submit();
         } else {
            trackAnalytics(false,'pagina-personalizada','erro');
         }
      });
   },

   setEls: function() {
      this.$form = $('#formCustomPost');
      this.$btnSend = $('#formCustomPostSend');
      this.$loadFormSend = $('#loadFormSend');
      this.$customLink = $('#customLink');
      this.$rede = $('#txtrede');
      this.$redeout = $('#lblredeout');
   },

   send: function() {
      var _that = this;

      this.$form.validate({
         onkeyup: false,
         rules: {
            nome: {
               blacklist : true,
               validaName: true,
               required: true,
               minlength: 3,
               maxlength: 50
            },
            telefone: {
               minlength: 14
            },
            rede: {
               validaRede: true
            }
         },
         errorPlacement: function(error, element) {
            if( MaterialDivugacao.lastError != error.html()){
               MaterialDivugacao.lastError = error.html();
               // Removido para evitar muitos erros.
               // trackAnalytics(false,'pagina-personalizada','erro');
            }
         },
         submitHandler: function(form) {
            _that.$form.addClass('sending');

            var success = function(anchor) {
               Modal.open({
                  el: '#formSuccess',
                  autoClose: 2500,
                  callback: function(result) {
                     ScrollAnchor.go(anchor);
                     _that.$form.removeClass('sending');
                     _that.getEmailCustom();
                     _that.getCoversCustom();
                  }
               });
            };

            // Le os campos do formulario
            var dataForm = Utils.getParamsObj(_that.$form.serialize());

            // Normaliza a rede do usuario
            var _rede = _that.matchRede();
            if (_rede != null) {
               dataForm.rede = _rede;
            }

            // ADICIONA DADOS DO FORM AO LOCALSTORAGE
            localStorage.setItem("natura_dia_das_maes", _that.$form.serialize());
            // $.ajax({
            //    method: 'POST',
            //    url: PersistData.absUrl + "/base64encode.php",
            //    data: {
            //       data: _that.$form.serialize()
            //    },
            //    success: function(result){
            //       localStorage.setItem("natura_dia_das_maes", result);
            //       _that.getEmailCustom();
            //    }
            // });


            var cookie = Cookie.get('natura_dia_das_maes');
            if (cookie)
               Cookie.addItem('natura_dia_das_maes', dataForm);
            else
               Cookie.set('natura_dia_das_maes', dataForm);

            // Le os dados do formulario e normaliza a rede do usuario
            var data = Utils.getCookieData();
            var foneJustNumbers = Utils.clearPhoneNumber(dataForm.telefone);
            var utmtag = '&utm_source=paginapersonalizada&utm_medium='+foneJustNumbers+'&utm_content=ativacao-toolkit&utm_campaign=natal-toolkit';

            // ScrollAnchor.go('#loadFormSend');

            GoogleApiUrl.getShordUrl({
               url: PersistData.siteCFUrl + data.rede + "?data=" + Utils.getHash(data) + utmtag,

               success: function(url) {
                  // SalvarCn.saveData($.extend({link:url},dataForm));
                  trackAnalytics(false,'pagina-personalizada','sucesso');

                  Cookie.addItem('natura_dia_das_maes', {
                     goo: url.replace('http://goo.gl/', '')
                  });

                  _that.applyTrackData(dataForm);
                  // _that.$customLink.find('.sc-form-ipt').val(url);
                  _that.$loadFormSend.addClass('sc-none');
                  // _that.$customLink.removeClass('sc-none');
                  success('#whatsapp-personal-anchor');
               }
            });

            // if (!!dataForm.rede) {
            //   _that.$loadFormSend.removeClass('sc-none');

            // } else {
            //   // ScrollAnchor.go('#passo02');
            //   _that.applyTrackData(dataForm);
            //   success('#passo02');
            // }
         }
      });
   },

   getEmailCustom: function() {
      var emailPostId = 'E01';
      var $emailCustom = $('.box-email-custom');

      // Create Base64 Object
      var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

      $('.box-email').addClass('sc-none');
      $emailCustom.addClass('loading');

      //var url = Utils.getImagePath(emailPostId, Utils.getCookieData());
      var url = Utils.getImagePath(emailPostId, '') + '&' + localStorage.getItem('natura_dia_das_maes');
      var urlDownload = url+'&download=true';

      //var teste = new Preload([url], function() {
      $emailCustom.removeClass('loading');
      var $img = $emailCustom.find('img'),
      $a = $emailCustom.find('a');

      $img.attr('src', url + '&random='+Math.random() );
      $a.attr('href', urlDownload);

      setTimeout(function() {
         $emailCustom.addClass('actived');
      }, 500);
      // });
   },

  getCoversCustom: function() {
    var $item = $('.box-cover-item'),
        urls = [
          Utils.getImagePath('F01', Utils.getCookieData()),
          Utils.getImagePath('F02', Utils.getCookieData()),
          Utils.getImagePath('F03', Utils.getCookieData())
        ];

    $item.addClass('loading');
    var teste = new Preload(urls, function() {
      $.each(urls, function(i) {
        var urlImg = urls[i];
        var urlDownload = urlImg+'&download=true';

        $item.eq(i).find('.sc-img-wrap .sc-img').attr('src', urlImg); // imagem
        $item.eq(i).find('.sc-img-wrap a').attr('href', urlDownload); // link
        setTimeout(function() {
          $item.eq(i).removeClass('loading');
        }, 1000);
      });
    });
  },
  applyMaskForm: function() {
    var $formNumCont = $('#formNumCont');
    $formNumCont.mask('(00) 00000-0000')
                .blur(function() {
                  var target, phone, element;
                  target = (event.currentTarget) ? event.currentTarget : event.srcElement;
                  phone = target.value.replace(/\D/g, '');
                  element = $(target);
                  console.log('blur', phone.length);
                  $formNumCont.unmask();
                  if(phone.length === 10) {
                    $formNumCont.mask('(00) 0000-0000');
                  } else {
                    $formNumCont.mask('(00) 00000-0000');
                  }
                });
  },
  applyTrackData: function(data) {
    console.log('applyTrackData');
    var _that = this;
    $('#fbCustomPosts .sc-item, #wpCustomPosts .sc-item').each(function() {
      var $target = $(this).find('.sc-item-lbl');
      if ($target.length)
        $target.remove();

      $(this).prepend(Utils.getTemplateTrack(data));
    });
  },
  fillDataIfExist: function() {
    var cookie = Cookie.get('natura_natal');
    if (cookie) {
      if (!!cookie.nome && !!cookie.telefone) {
        var telefone = cookie.telefone,
            nome = cookie.nome.replace(/\+/g, ' '),
            goo = (typeof cookie.goo !== "undefined") ? cookie.goo : null;
        this.applyTrackData(cookie);
        this.getEmailCustom();
        this.getCoversCustom();

        // if (goo) {
        //   this.$customLink.removeClass('sc-none');
        //   this.$customLink.find('.sc-form-ipt').val('http://goo.gl/' + goo);
        // }

        $('#formCustomPost .sc-form-ipt[name="nome"]').val(decodeURIComponent(nome));
        $('#formCustomPost .sc-form-ipt[name="telefone"]').unmask();
        $('#formCustomPost .sc-form-ipt[name="telefone"]').val(Utils.formatPhoneNumber("55" + Utils.clearPhoneNumber(telefone)));
      }
    }
  },
  validaName: function() {
    $.validator.addMethod("validaName", function(value, element) {
      var reg = /^([a-zA-Z ']*)$/;
      var value = $.trim(value);
      if (value.indexOf(" ") !== -1) {
        var arr = value.split(' ');
        if (arr[0].length > 15)
          return false;
      } else if (value.length > 15) {
        return false;
      } else if ( !reg.test(value) ){
        return false;
      }
      return true;
    }, "Nome inválido");
  },
  validaRede: function(){
    var _that = this;
    $.validator.addMethod("validaRede", function(value, element) {
      _that.$redeout.html("");
      var str   = $.trim(value);
      var empty = str.length < 1;
      if (empty) {
        return true;
      }
      // check
      var m     = _that.matchRede();
      var valid = (m != null);
      if (valid) {
        // print full url
        var out = "http://rede.natura.net/espaco/" + m + "/"
        _that.$redeout.html(out);
      }
      return valid;
    }, "Rede inválida");
  }
};

window.MaterialDivugacao = MaterialDivugacao;

export default MaterialDivugacao;
