var data = require('../data-content/posts');
/**
 Arquivo com os dados que serao aplicados
 no HTML pre-compilado do ambiente local.
 @name Config
*/
module.exports = {

  /**
   Titulo da pagina
   @property {string} title Titulo da pagina
  */
  title: "Natura Dia das Mães",

  /**
   Descricao da pagina
   @property {string} description Descricao da pagina
  */
  description: "Descricao da pagina",

  /**
   Url base da pagina. Ela varia no ambiente de producao,
   podendo ser relativa
   @property {string} baseUrl Base de url do projeto
  */
  baseUrl: "/www",

  /**
   Tambem e base URL do projeto, mas sempre deve ser usada
   com o endereco absoluto. Muito util para usar nos projetos
   com integracao de APIs como Facebook (para post de arquivos)
   @property {string} absUrl Base de url do projeto com endereco absoluto
  */
  absUrl: 'http://187.191.99.159',

  /**
   Url de servico backend. Deve ser utilizada em
   requisições ajax na aplicacao
   @property {string} serviceUrl Url do servico
  */
  serviceUrl: "http://187.191.99.159/image.php",
  serviceUrlSaveCn: "http://187.191.99.159/cndb/data.php",
  // serviceUrl: "http://salveqa.com.br/natura-toolkit-natal-be/image.php",

  serviceUrlWhats: "https://329lxrolmg.execute-api.us-east-1.amazonaws.com/prod/whats",

  siteCFUrl: "http://rede.natura.net/espaco/",
  //siteCFUrl: "http://natura.com.br/natal/",
  /**
   Numero da versao do pacote. Essa propriedade e usada
   nos pacotes de stage e producao. Seu valor sera concatenado
   com a propriedade assetsPath para gerar o caminho
   do pacote.
   @property {string} version Versao do pacote
  */
  version: "",

  /**
   Caminho dos assets do projeto. Essa propriedade e usada
   para gerar o caminho do pacote para os ambientes
   stage e salveqa. Os arquivos de assets serao copiados
   dentro do caminho decladado nessa propriedade concatenado
   ao valor declarado na propriedade de versao. Ex.:
   version: "v1",
   assetsPath: "static/teste/assets",

   Os diretorios dos assets devem ficar assim:
   static/teste/assets/v1/img
   static/teste/assets/v1/js
   static/teste/assets/v1/css

   @property {string} assetsPath Caminho do pacote
  */
  assetsPath: "",

  /**
   Extencao da pagina deve ser usada quando o projeto
   possuir mais de uma pagina. Isso facilita
   a aplicacao de links, pois o desenvolvedor podera
   trabalhar simulando o ambiente de producao ou stage.
   Esse valor deve ser vazio nos nas configuracoes
   de stage e producao. A aplicacao esta no exemplo:
   <a href="{{baseUrl}}/outrapagina{{extPage}}">

   Para o ambiente local, o html ficaria assim:
   <a href="http://localhost:3000/outrapagina.html">

   Assim a navegacao se torna possivel, pois nos ambientes
   de prod e salveqa, nao temos essa extencao. Entao nos
   htmls compilados do seus ambientes esse mesmo link
   fica assim:
   <a href="/static/nome-do-projeto/assets/v1/outrapagina">

   @property {string} extPage Extencao que sera renderizada no html
  */
  extPage: ".html",

  /**
   Controla a exibicao do header e footer que simula o
   ambiente de producao. Essa propriedade deve ser
   falsa nos ambientes de stage e producao
   @property {boolean} hasHeader
  */
  hasHeader: true,

  /**
   Facebook ID para usar nas aplicacoes com integracao
   @propriety {string} Numero do ID do Facebook
  */
  fb_id: "1113206468721896",

  /**
   Nome do album que serao armazenados os posts realizados pela campanha
   @propriety {string} Numero do ID do Facebook
  */
  fbAlbumName: "Dia das Mães Natura",

  /**
   Numero da chave para usar a API do
   google encurtador de URL
   @param googleApiKey
  */
  googleApiKey: "AIzaSyAkl-UsosFU4XF-jR2KARVai4KaFztYK8c",

  /**
   Url de redirecionamento da versao mobile.
   Usado em Natura para versao temporaria enquanto
   a versao desktop nao esta completamente responsiva
   @property {string} Url da versao mobie
  */
  mbUrl: "/www/mobile",

  /**
   Dados para serem aplicados no html
   @param googleApiKey
  */


  gaDefaultCategory: 'dia-das-maes-toolkit',

  content: data
}
