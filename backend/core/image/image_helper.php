<?php
/**
 * Created by PhpStorm.
 * User: Valdek
 * Date: 03/10/2015
 * Time: 16:06
 */

namespace Core\Image;

class Size {
    public $width;
    public $height;
    function __construct($w, $h) {
        $this->width = $w;
        $this->height = $h;
    }
}

class Point {
    public $x;
    public $y;
    function __construct($x, $y) {
        $this->x = $x;
        $this->y = $y;
    }
}

class Color {

    public $red;
    public $green;
    public $blue;
    public $hex;

    function __construct() {
        $this->red = FALSE;
        $this->green = FALSE;
        $this->blue = FALSE;
        $this->hex = FALSE;
    }

    public function value() {
        if (FALSE === $this->hex) {
            return "#" . $this->red . $this->green . $this->blue;
        } else {
            return $this->hex;
        }
    }

    public static function fromHex($hex) {
        $color = new Color();
        $color->hex = $hex;
        return $color;
    }

    public static function fromRGB($r, $g, $b) {
        $color = new Color();
        $color->red = $r;
        $color->green = $g;
        $color->blue = $b;
        return $color;
    }

}

class Font {
    public $ttf;
    public $size;
    function __construct() {
        $this->ttf = FALSE;
        $this->size = 4;
    }
    public function isTTF() {
        return FALSE !== $this->ttf;
    }
}

class LabelLine {

    public $font;
    public $content;
    public $foregroundColor;
    public $point;

    function __construct() {
        $this->font = new Font();
        $this->content = "Hello World";
        $this->foregroundColor = FALSE;
        $this->point = new Point(0,0);
    }

}

class LabelArguments {

    public $labelLineArray; // label line contents
    public $backgroundColor; // color
    public $foregroundColor; // color
    public $size; // Size
    public $font; // Font file name
    public $horizontalMargin;
    function __construct() {
        $this->labelLineArray = array();
        $this->backgroundColor = Color::fromHex("#000000");
        $this->foregroundColor = Color::fromHex("#ffffff");
        $this->size = new Size(800, 800);
        $this->font = new Font();
        $this->horizontalMargin = 5;
    }
}

class GeneratedImage {
    public $resource;
    public $width;
    public $height;
    function __construct($resource, $width, $height) {
        $this->resource = $resource;
        $this->width    = $width;
        $this->height   = $height;
    }
}

class ImageHelper {

    public function createLabel($labelArguments) {

        // Create image
        $userWidth = $labelArguments->size->width;
        $calculatedWidth = $this->maxWidth($labelArguments);
        $width = max ( $userWidth, $calculatedWidth );
        $height = $labelArguments->size->height;

        $resource = imagecreatetruecolor( $width, $height )
        or die("GD_ERROR");

        // Background
        $bkgcolor = $labelArguments->backgroundColor;
        $background_color = imagecolorallocate($resource, $bkgcolor->red, $bkgcolor->green, $bkgcolor->blue);
        imagefill($resource, 0, 0, $background_color);

        // Write Lines
        foreach($labelArguments->labelLineArray as $line) {
            $this->printString($resource, $labelArguments, $line);
        }

        return new GeneratedImage($resource, $width, $height);
    }

    protected function maxWidth($labelArguments) {
        $maxWidth = 0;
        foreach($labelArguments->labelLineArray as $line) {
            $textWidth = $this->textWidth($labelArguments, $line);
            $maxWidth = max($maxWidth , $textWidth);
        }
        return $maxWidth + ($labelArguments->horizontalMargin * 2);
    }

    protected function textWidth($labelArguments, $line) {
        $ttf_font = $this->ttfFont($labelArguments, $line);
        if ( FALSE === $ttf_font ) {
            return imagefontwidth( $line->font->size );
        } else {
            /**
             * http://php.net/manual/en/function.imagettfbbox.php
             */
            $size = imagettfbbox( $line->font->size, 0, $ttf_font, $line->content );
            return $size[2] - $size[0];
        }
    }

    /**
     * Imprime uma linha na Imagem
     * @param $resource
     * @param $labelArguments
     * @param $line
     */
    protected function printString($resource, $labelArguments, $line) {
        // Font TTF priority
        //  1) line font
        //  2) label font
        //  3) normal font

        // Check line font
        $ttf_font = $this->ttfFont($labelArguments, $line);

        // Color
        $textcolor = $this->textColor($resource, $labelArguments, $line);

        // Check Print Type
        if ( FALSE === $ttf_font ) {
            $this->printBuiltIn($resource, $labelArguments, $line, $textcolor);
        } else {
            $this->printTTF($resource, $labelArguments, $line, $textcolor, $ttf_font);
        }
    }

    /**
     * Retorna a cor do texto com base na cor geral (LabelArguments) ou na cor da linha (LabelLine)
     * @param $resource
     * @param $labelArguments
     * @param $line
     * @return int
     */
    protected function textColor($resource, $labelArguments, $line) {
        $textforecolor = (FALSE === $line->foregroundColor)
            ? $labelArguments->foregroundColor
            : $line->foregroundColor;
        $textcolor = imagecolorallocate($resource, $textforecolor->red,
            $textforecolor->green, $textforecolor->blue);
        return $textcolor;
    }

    protected function xPosition($labelArguments, $line) {
        return $labelArguments->horizontalMargin + $line->point->x;
    }

    /**
     * Imprime um texto em uma imagem usando a fonte padrao do PHP
     * @param $resource
     * @param $labelArguments
     * @param $line
     * @param $textcolor
     */
    protected function printBuiltIn($resource, $labelArguments, $line, $textcolor) {
        imagestring($resource,
            $line->font->size, $this->xPosition($labelArguments, $line), $line->point->y,
            $line->content, $textcolor);
    }

    /**
     * Imprime um texto em uma imagem usando TTF
     * @param $resource
     * @param $labelArguments
     * @param $line
     * @param $textcolor
     * @param $ttf_font
     */
    protected function printTTF($resource, $labelArguments, $line, $textcolor, $ttf_font) {
        imagettftext( $resource,
            $line->font->size, 0, $this->xPosition($labelArguments, $line), $line->point->y,
            $textcolor, $ttf_font, $line->content);
    }

    /**
     * Retorna a fonte TTF quando houver. Se nao houver, retorna FALSE
     * @param $labelArguments
     * @param $line
     * @return bool
     */
    protected function ttfFont($labelArguments, $line) {
        $ttf_font = FALSE;
        $isTTF = $line->font->isTTF() || $labelArguments->font->isTTF();
        if ( TRUE === $isTTF ) {
            $ttf_font = $line->font->isTTF()
                ? $line->font->ttf
                : $labelArguments->font->ttf;
        }
        return $ttf_font;
    }

    public static function defineSendFile($filename, $download = FALSE) {
        $final_filename = ImageHelper::changeFilename($filename);
        header("Content-type: image/jpg");
        if ($download) {
            header('Content-Disposition: attachment; filename="' . $final_filename . '"');
        }else{
            header('Content-Disposition: filename="' . $final_filename . '"');
        }
    }

    public static function changeFilename($filename) {
        $arr = array(
            'email_01.jpg' => 'Natura_email_01.jpg',
            'face_01.jpg'  => 'Natura_capa_01.jpg',
            'face_02.jpg'  => 'Natura_capa_02.jpg',
            'face_03.jpg'  => 'Natura_capa_03.jpg'
        );
        foreach($arr as $k => $v) {
            $imgname = "images/$k";
            if ($imgname === $filename) {
                return $v;
            }
        }
        return $filename;
    }

    public static function renderHttpImage($labelImage, $filename = "natura.jpg", $download = FALSE) {
        // Define o nome do arquivo e identifica se este arquivo devera
        // ser enviado como download.
        ImageHelper::defineSendFile($filename, $download);
        imagejpeg($labelImage, null, 90);
    }

    public static function readFromFile($filename, &$width, &$height) {
        // http://php.net/manual/en/function.getimagesize.php
        $size   = getimagesize($filename);
        $width  = $size[0]; // index 0=width
        $height = $size[1]; // index 1=height
        switch($size["mime"]) {
            case "image/jpeg":
                $resource = imagecreatefromjpeg($filename);
                break;
            case "image/png":
                $resource = imagecreatefrompng($filename);
                break;
            default:
                return FALSE;
                break;
        }
        return $resource;
    }

}

class ImageRedim {

    protected $types;

    function __construct() {
        $this->types = array();
//        $this->types = array(
//            's10' => 0.1,
//            's20' => 0.2,
//            's30' => 0.3,
//            's40' => 0.4,
//            's50' => 0.5,
//            's60' => 0.6,
//            's70' => 0.7,
//            's80' => 0.8,
//            's90' => 0.9,
//            's200' => 2.0,
//            'w' => 0.5
//        );
    }

    public function redim($resource, $type) {
        // Se nao houver um tipo, retorna o proprio resource
        $check = FALSE;
        foreach($this->types as $k => $v) {
            if ($k === $type) {
                $check = TRUE;
                break;
            }
        }
        if (FALSE === $check) {
            return $resource;
        }
        // Width factor
        $factor = $this->types[$type];
        $w = floatval(imagesx($resource)) * $factor;
        $h = floatval(imagesy($resource)) * $factor;
        // Insane check
        if ($w <= 0 || $h <= 0) {
            return $resource;
        }
        // Scale
        $newresource = imagescale($resource, $w, $h);
        imagedestroy($resource);
        return $newresource;
    }

}
