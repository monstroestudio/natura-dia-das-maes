<?php
/**
 * Created by PhpStorm.
 * User: Valdek
 * Date: 03/10/2015
 * Time: 20:05
 */

namespace Core\Image\Natura;

require_once(__DIR__ . "/../image_helper.php");

class NaturaLabelHandler {

    public function createLabel ($nome, $telefone, $rede) {
        // Valid Fields
        $invalid = FALSE === $nome || $telefone === FALSE;
        $hasRede = FALSE !== $rede;

        /**
         * Fix values and create lines
         */
        $lines = array();

        /**
         * Treat lines
         */
        if ( FALSE === $invalid ) {

            $y = 30;
            $x = 0;
            $lineoffset = 25;

            // Line 1
            $nome = strtoupper($nome);
            $line1 = new \Core\Image\LabelLine();
            $line1->content  = "COMPRE COM $nome";
            $line1->point = new \Core\Image\Point($x, $y);
            $line1->font->ttf = "fonts/OpenSans-Bold.ttf";
            $line1->font->size = 12;
            array_push($lines, $line1);

            // Line 2
            $line2 = new \Core\Image\LabelLine();
            $line2->content = "Ligue para $telefone";
            $line2->point = new \Core\Image\Point($x, $y += $lineoffset);
            $line2->font->ttf = "fonts/OpenSans-Regular.ttf";
            $line2->font->size = 11;
            array_push($lines, $line2);

            // Line 3
            // if (TRUE === $hasRede) {
            //     $rede = strtolower($rede);
            //     $line3 = new \Core\Image\LabelLine();
            //     $line3->content = "Ou acesse $rede";
            //     $line3->point = new \Core\Image\Point($x, $y += $lineoffset);
            //     $line3->font->ttf = "fonts/OpenSans-Regular.ttf";
            //     $line3->font->size = 9;
            //     array_push($lines, $line3);
            // }
        }

        /**
         * Generate Image
         */
        $labelArguments = new \Core\Image\LabelArguments();
        $labelArguments->size = new \Core\Image\Size(IMAGE_WIDTH, IMAGE_HEIGHT);
        $labelArguments->backgroundColor = \Core\Image\Color::fromRGB(152, 39, 45);
        $labelArguments->foregroundColor = \Core\Image\Color::fromRGB(255, 255, 255);
        $labelArguments->labelLineArray = $lines;
        $labelArguments->horizontalMargin = 10;

        $ih = new \Core\Image\ImageHelper();
        $labelImage = $ih->createLabel($labelArguments);
        return $labelImage;
    }

}
