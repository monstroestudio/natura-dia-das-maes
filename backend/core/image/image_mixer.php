<?php
/**
 * Created by PhpStorm.
 * User: Valdek
 * Date: 03/10/2015
 * Time: 20:15
 */

namespace Core\Image;

use Core\Image\Natura\NaturaLabelHandler;

require_once(__DIR__ . "/image_helper.php");
require_once(__DIR__ . "/natura/naturalabelhandler.php");

class ImageMixerArguments {

    public $originalFilename;
    public $startPoint;
    public $generatedSecondImage;
    function __construct($filename, $startPoint, $genSecondImage) {
        $this->originalFilename = $filename;
        $this->startPoint = $startPoint;
        $this->generatedSecondImage = $genSecondImage;
    }

}

class ImageMixer {

    public function mix ($imageMixerArguments) {
        // Read image
        $filename = $imageMixerArguments->originalFilename;
        $resource = ImageHelper::readFromFile($filename, $width, $height);
        if ( !$resource ) {
            return FALSE;
        }

        // Copy Image
        imagecopy( $resource,
            $imageMixerArguments->generatedSecondImage->resource,
            $imageMixerArguments->startPoint->x, // destination x
            $imageMixerArguments->startPoint->y, // destination y
            0, 0, // start x and y
            $imageMixerArguments->generatedSecondImage->width,
            $imageMixerArguments->generatedSecondImage->height);

        return new GeneratedImage($resource, $width, $height);
    }

}