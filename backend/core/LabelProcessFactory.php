<?php
/**
 * Created by PhpStorm.
 * User: Valdek
 * Date: 11/10/2015
 * Time: 17:25
 */

namespace Core;

class LabelProcessFactory {

    public function create($labelDataReader) {
        // Check by image id
        $postid = mb_strtoupper( $labelDataReader->idImagem );
        $valid = is_string($postid) && strlen($postid) > 0;
        if (FALSE === $valid) {
            return FALSE;
        }
        $first = substr($postid, 0, 1);
        switch($first) {
            case "F":
                return new FacebookLabelProcessHelper();
                break;
            case "E":
                return new EmailLabelProcessHelper();
                break;
            default:
                //return new WhatsAppLabelProcessHelper();
                return new PostLabelProcessHelper();

//                // Check is WHATSAPP
//                if (TRUE === $this->isWhatsApp($labelDataReader)) {
//                    return new WhatsAppLabelProcessHelper();
//                } else {
//                    return new PostLabelProcessHelper();
//                }

                break;
        }
    }

    protected function isWhatsApp($labelDataReader) {
        return "w" === $labelDataReader->type;
    }

}