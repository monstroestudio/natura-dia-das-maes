<?php
/**
 * Created by PhpStorm.
 * User: Valdek
 * Date: 11/10/2015
 * Time: 16:10
 */

namespace Core;

use Core\Image\Color;

abstract class AbstractLabelProcessHelper {

    const Y_THREE_LINES = 27;
    const Y_TWO_LINES = 38;

    public $labelY;
    public $labelX;
    public $labelOffset;
    public $height;
    public $horizontalMargin;
    protected $lines;
    protected $forecolor;
    protected $backcolor;

    function __construct() {
        $this->lines = array();
        $this->labelX = 0;
        $this->labelY = AbstractLabelProcessHelper::Y_THREE_LINES;
        $this->labelOffset = 20;
        $this->height = 90;
        $this->horizontalMargin = 0;
        $this->backcolor = \Core\Image\Color::fromRGB(204, 164, 167);
        $this->forecolor = \Core\Image\Color::fromRGB(255, 255, 255);
        $this->defineParameters();
    }

    protected abstract function defineParameters();

    protected abstract function buildData($labelDataHelper);

    protected function createLine($text, $size, $x, $y, $isBold = FALSE) {
        $fonte = (TRUE === $isBold) ? "GillSansStd_Bold.ttf" : "GillSansStd_Regular.ttf";
        $fonte = "fonts/$fonte";
        $line = new \Core\Image\LabelLine();
        $line->content = $text;
        $line->point = new \Core\Image\Point($x, $y);
        $line->font->ttf = $fonte;
        $line->font->size = $size;
        array_push($this->lines, $line);
        return $line;
    }

    public function createLabel($labelDataHelper) {
        $this->buildData($labelDataHelper);
        $labelArgs = new \Core\Image\LabelArguments();
        $labelArgs->labelLineArray = $this->lines;
        $labelArgs->size = new \Core\Image\Size(0, $this->height);
        $labelArgs->horizontalMargin = $this->horizontalMargin;
        $labelArgs->backgroundColor = $this->backcolor;
        $labelArgs->foregroundColor = $this->forecolor;
        $labelHelper = new \Core\Image\ImageHelper();
        $labelImage = $labelHelper->createLabel($labelArgs);
        return $labelImage;
    }

    protected function calculatedOffset() {
        return $this->labelOffset * count($this->lines);
    }

    protected function yWithOffset() {
        return $this->labelY + $this->calculatedOffset();
    }

}

abstract class AbstractLabelHelper extends AbstractLabelProcessHelper {

    protected function createLineNome($labelDataHelper) {
        $nome = $labelDataHelper->nome;
        $this->createLine( "COMPRE COM $nome",
            14, $this->labelX, $this->yWithOffset(), TRUE );
    }

    protected function createLineTelefone($labelDataHelper) {
        $telefone = $labelDataHelper->telefone;
        if (!$telefone) {
            return;
        }

        $this->createLine( "Ligue para $telefone", 12, $this->labelX, $this->yWithOffset(), FALSE );
    }

    protected function createLineTelefone2($labelDataHelper) {
        $telefone = $labelDataHelper->telefone;
        if (!$telefone) {
            return;
        }

        if (!empty($labelDataHelper->rede)) {
          $this->createLine( "ou ligue para $telefone", 12, $this->labelX, $this->yWithOffset(), FALSE );
        } else {
          $this->createLine( "Ligue para $telefone", 12, $this->labelX, $this->yWithOffset(), FALSE );
        }
    }

    protected function createLineGoo($labelDataHelper) {
        $goo = $labelDataHelper->goo;
        if (!empty($labelDataHelper->rede)) {
          $this->createLine( "Acesse rede.natura.net/espaco/$labelDataHelper->rede", 12, $this->labelX, $this->yWithOffset(), FALSE );
        }

        //$this->createLine( "Ou acesse http://goo.gl/$goo", 12, $this->labelX, $this->yWithOffset(), FALSE );
    }

    protected function createLineGoo2($labelDataHelper) {
        $goo = $labelDataHelper->goo;

        if (!empty($labelDataHelper->rede)) {
          $this->createLine( "Acesse:", 12, $this->labelX, $this->yWithOffset(), FALSE );
          $this->createLine( "rede.natura.net/espaco/$labelDataHelper->rede", 11, $this->labelX, $this->yWithOffset(), FALSE );
       } else {
          $this->createLine( " ", 12, $this->labelX, $this->yWithOffset(), FALSE );
       }

        //$this->createLine( "Ou acesse http://goo.gl/$goo", 12, $this->labelX, $this->yWithOffset(), FALSE );
    }

    protected function buildData($labelDataHelper) {
        $this->createLineNome($labelDataHelper);
        $this->createLineTelefone($labelDataHelper);
        $this->createLineGoo($labelDataHelper);
    }

    protected function defineParameters() {
        $this->horizontalMargin = 20;
    }

}

class PostLabelProcessHelper extends AbstractLabelHelper {

}

class WhatsAppLabelProcessHelper extends AbstractLabelHelper {

    /**
     * Aguardando definicao se sera apenas duas linhas.
     * Se for apenas duas linhas, basta apenas descomentar
     * o codigo abaixo.
    */
    protected function defineParameters() {
        parent::defineParameters();
        $this->labelY = WhatsAppLabelProcessHelper::Y_TWO_LINES;
    }

    protected function buildData($labelDataHelper) {
        $this->createLineNome($labelDataHelper);
        $this->createLineTelefone($labelDataHelper);
    }

}

class FacebookLabelProcessHelper extends AbstractLabelHelper {
   protected function defineParameters() {
      parent::defineParameters();
      $this->backcolor = \Core\Image\Color::fromRGB(111, 98, 90);
      $this->forecolor = \Core\Image\Color::fromRGB(255, 255, 255);
   }
}

class EmailLabelProcessHelper extends AbstractLabelHelper {

    protected function defineParameters() {
        parent::defineParameters();
        $this->horizontalMargin = 0;
        $this->backcolor = \Core\Image\Color::fromRGB(255, 255, 255);
        $this->forecolor = \Core\Image\Color::fromRGB(111, 98, 90);
    }

    protected function buildData($labelDataHelper) {
        $this->createLineGoo2($labelDataHelper);
        $this->createLineTelefone2($labelDataHelper);
    }

}
