<?php
/**
 * Created by PhpStorm.
 * User: Valdek
 * Date: 13/10/2015
 * Time: 04:40
 */

require_once(__DIR__ . "/cors_helper.php");
require_once(__DIR__ . "/req_helper.php");
require_once(__DIR__ . "/../image/image_helper.php");
require_once(__DIR__ . "/../image/image_mixer.php");
require_once(__DIR__ . "/../BlacklistHelper.php");
require_once(__DIR__ . "/../LabelDataReader.php");
require_once(__DIR__ . "/../LabelProcessHelper.php");
require_once(__DIR__ . "/../LabelProcessFactory.php");

/**
 * Geracao de Imagem
 * Para gerar a imagem, precisamos de:
 * 1) id da imagem, que possui os dados da imagem
 * 2) {opcional} dados da imagem (nome, telefone, redenatura)
 * */
$labelDataReader = new \Core\LabelDataReader();
if (FALSE === $labelDataReader->valido) {
    exit();
}

/** Gera a Etiqueta */
$factory = new \Core\LabelProcessFactory();
$helper  = $factory->create($labelDataReader);
$labelImage = $helper->createLabel($labelDataReader);