<?php
/**
 * Created by PhpStorm.
 * User: Valdek
 * Date: 03/10/2015
 * Time: 16:57
 */

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

require_once(__DIR__ . "/core/image/image_helper.php");
require_once(__DIR__ . "/core/image/image_mixer.php");
require_once(__DIR__ . "/core/util/req_helper.php");
require_once(__DIR__ . "/core/util/const_helper.php");
require_once(__DIR__ . "/core/image/natura/naturalabelhandler.php");

// Read Fields
$data     = _req_get("data", FALSE);
$nome     = "";
$telefone = "";
$rede     = "";
if ( FALSE !== $data ) {
    // Base 64 data
    $base64data = base64_decode($data);
    $rawdata = explode("&", $base64data);
    foreach($rawdata as $rawinfo) {
        $info = explode("=", $rawinfo);
        if ( count($info) !== 2 ) {
            continue;
        }
        $key   = urldecode($info[0]); // key
        $value = urldecode($info[1]) ; // value
        switch($key) {
            case "nome":
                $nome = $value;
                break;
            case "telefone":
                $telefone = $value;
                break;
            case "rede":
                $rede = $value;
                break;
        }
    }
} else {
    // Check raw data is OK
    $rawdata  = _req_get("rawdata", "");
    if ( "1" === $rawdata ) {
        $nome     = _req_get("nome", "");
        $telefone = _req_get("telefone", "");
        $rede     = _req_get("rede", "");
    }
}

// Check Valid
$invalid = $nome === "" || $telefone === "";
if ($invalid) {
    header("Content-type: image/png");
    header('Location: images/piloto.png');
}

// Natura
$o = new \Core\Image\Natura\NaturaLabelHandler();
$gen   = $o->createLabel($nome, $telefone, $rede);

// Mix
$o = new \Core\Image\ImageMixer();
$bigGen = $o->mix(
    new \Core\Image\ImageMixerArguments(
        "images/piloto.png",
        new \Core\Image\Point(0, 255),
        $gen ) );

/**
 * Ignorar todos os erros, afinal se houver erro a imagem
 * nao ira aparecer de qualquer forma
 */
//// Check error
//$errors = error_get_last();
//
//// Is PHP_INTL error
//$has_errors = NULL == $errors && is_array($errors) && count($errors) > 0;
//$is_intl_error = TRUE === $has_errors && FALSE !== stripos( $errors[0]["message"], "php_intl" );
//// When error occurred and it is not an INTL error, print error and exit.
//if ( $has_errors ) {
//    if (FALSE !== $is_intl_error) {
//        print_r($errors);
//    }
//}

// No errors or only INTL error
\Core\Image\ImageHelper::renderHttpImage($bigGen->resource);

// Mata as imagens geradas
imagedestroy($gen->resource);
imagedestroy($bigGen->resource);