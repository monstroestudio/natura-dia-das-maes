<?php
/**
 * Created by PhpStorm.
 * User: Valdek
 * Date: 09/10/2015
 * Time: 00:56
 */

/*
 * Trabalha na geracao apenas da imagem da etiqueta. O resto
 * depende da logica de cada modulo, este ira criar uma nova
 * imagem maior.
 * */


require_once(__DIR__ . "/core/util/cors_helper.php");
require_once(__DIR__ . "/core/util/req_helper.php");
require_once(__DIR__ . "/core/image/image_helper.php");
require_once(__DIR__ . "/core/image/image_mixer.php");
require_once(__DIR__ . "/core/BlacklistHelper.php");
require_once(__DIR__ . "/core/LabelDataReader.php");
require_once(__DIR__ . "/core/LabelProcessHelper.php");
require_once(__DIR__ . "/core/LabelProcessFactory.php");

/**
 * Geracao de Imagem
 * Para gerar a imagem, precisamos de:
 * 1) id da imagem, que possui os dados da imagem
 * 2) {opcional} dados da imagem (nome, telefone, redenatura)
 * */
$labelDataReader = new \Core\LabelDataReader();

// Verifica se o usuario quer fazer download da imagem.
$download = isset($_GET['download']);

// Caso os dados do usu�rio nao estejam OK, iremos redirecionar
// a pagina diretamente para a imagem solicitada.
if (FALSE === $labelDataReader->valido) {
    \Core\Image\ImageHelper::defineSendFile($labelDataReader->arquivo, $download);
    readfile($labelDataReader->arquivo);
    exit();
}

/** Gera a Etiqueta */
$factory = new \Core\LabelProcessFactory();
$helper  = $factory->create($labelDataReader);
$labelImage = $helper->createLabel($labelDataReader);

/** Cola a Etiqueta na imagem original */
$mixerArgs = new \Core\Image\ImageMixerArguments(
    $labelDataReader->arquivo,
    $labelDataReader->point,
    $labelImage);
$mixerHelper = new \Core\Image\ImageMixer();
$imagemMista = $mixerHelper->mix($mixerArgs);

/** Redimensiona a imagem */
$imageRedim = new \Core\Image\ImageRedim();
$resource = $imageRedim->redim($imagemMista->resource, $labelDataReader->type);

/** Render da imagem final */
\Core\Image\ImageHelper::renderHttpImage($resource, $labelDataReader->arquivo, $download);

/** Finaliza as variaveis */
imagedestroy($labelImage->resource);
imagedestroy($imagemMista->resource);